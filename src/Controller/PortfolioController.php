<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\cequetuveux;
use App\Entity\Projet;

class PortfolioController extends AbstractController
{
    #[Route('/', name: 'app_accueil')]
    public function index(): Response
    {
        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }

    #[Route('/portfolio', name: 'app_portfolio')]
    public function portfolio(ManagerRegistry $doctrine): Response
    {
        $Projet = $doctrine->getRepository(Projet::class)->findAll();
        // chercher les projets en base de donnees
        return $this->render('portfolio/index.html.twig', [
            'projets' => 'PortfolioController',
            'info' => $Projet
        ]);
    }
}
